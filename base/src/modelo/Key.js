import { Sound } from './src/modelo/Sound.js';

export class Key {

    constructor(name,nota,src){
     this._name=name;
     this._tipo=tipo;
     this._sound = null;
     this._DOMElement = null;
     this.initDOMElement();
    }

    get src(){
        return this._src;
    }
    set src(src){
        this._src = src;
    }

    get DOMElement(){
        return this._DOMElement;
    }
    set src(el){
        this._DOMElement = el;
    }

    initDOMElement(){
      let keyEl=document.createElement("div");
      if(this.tipo.includes("blanco")){
        keyEl.className="teclaBlanca piano"+this._name;
      }else{
        keyEl.className="teclaNegra piano-".this_name;
      }
      audioEl.id="piano-".this._name;
      audioEl.src=this._src;
      this._DOMElement=audioEl;
    }

    play(){
        this._DOMElement.play();
    }
    pause(){
        this._DOMElement.pause();
    }

}
