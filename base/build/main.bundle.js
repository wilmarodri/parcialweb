/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Album = __webpack_require__(2);

$(function () {

    var menu = $("#menu");
    var aside = $("aside");
    var items = $(".item");
    var audios = [];

    /* llenar elementos */
    loadItems();

    menu.click(function () {
        var val = aside.css("margin-left") == 0 || aside.css("margin-left") == "0px" ? "-205px" : 0;
        console.log(val);
        aside.animate({
            marginLeft: val
        }, 500, function () {
            // Animation complete.
        });
    });

    function loadItems() {
        catalog.forEach(function (album) {

            var a = new _Album.Album(album.name, album.img, album.desc, album.score, album.songs);
            console.log(a);
            a.addTo($("#items")[0]);
        }, this);

        console.log(audios);
    }
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Album = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Audio = __webpack_require__(3);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Album = exports.Album = function () {
    function Album(name, img, desc, score, songs) {
        _classCallCheck(this, Album);

        this._name = name;
        this._img = img;
        this._desc = desc;
        this._score = score;
        this._songs = [];
        this._DOMElement = null;
        this.initDOMElement();
        this.initSongs(songs);
    }

    _createClass(Album, [{
        key: "initSongs",
        value: function initSongs(songs) {
            songs.forEach(function (song) {

                var audio = new _Audio.Audio(song.name, song.src);
                this._songs.push(audio);

                this._DOMElement.appendChild(audio.DOMElement);
            }, this);
        }
    }, {
        key: "initDOMElement",
        value: function initDOMElement() {

            var loader = document.createElement("div");
            loader.className = "loader";

            var item = document.createElement("div");
            item.className = "item";
            item.style.background = "url(" + this._img + ")";
            item.style.backgroundSize = "cover";

            item.appendChild(loader);

            this._DOMElement = item;

            this.initListeners();
        }
    }, {
        key: "initListeners",
        value: function initListeners() {
            var _this = this;

            $(this._DOMElement).mouseover(function () {
                _this._songs[0].play();
            }).mouseout(function () {
                _this._songs[0].pause();
            });
        }
    }, {
        key: "addTo",
        value: function addTo(container) {
            container.appendChild(this._DOMElement);
        }
    }]);

    return Album;
}();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Audio = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Item2 = __webpack_require__(4);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Audio = exports.Audio = function (_Item) {
    _inherits(Audio, _Item);

    function Audio(name, src) {
        _classCallCheck(this, Audio);

        var _this = _possibleConstructorReturn(this, (Audio.__proto__ || Object.getPrototypeOf(Audio)).call(this, name));

        _this._src = src;
        _this._DOMElement = null;
        _this.initDOMElement();
        return _this;
    }

    _createClass(Audio, [{
        key: "initDOMElement",
        value: function initDOMElement() {
            var audioEl = document.createElement("audio");
            audioEl.src = this._src;
            this._DOMElement = audioEl;
        }
    }, {
        key: "play",
        value: function play() {
            this._DOMElement.play();
        }
    }, {
        key: "pause",
        value: function pause() {
            this._DOMElement.pause();
        }
    }, {
        key: "src",
        get: function get() {
            return this._src;
        },
        set: function set(src) {
            this._src = src;
        }
    }, {
        key: "DOMElement",
        get: function get() {
            return this._DOMElement;
        },
        set: function set(el) {
            this._DOMElement = el;
        }
    }]);

    return Audio;
}(_Item2.Item);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Item = exports.Item = function () {
    function Item(name, thumb) {
        _classCallCheck(this, Item);

        this._name = name;
        this._thumb = thumb;
    }

    _createClass(Item, [{
        key: "name",
        get: function get() {
            return this._name;
        },
        set: function set(name) {
            this._name = name;
        }
    }, {
        key: "thumb",
        get: function get() {
            return this._thumb;
        },
        set: function set(thumb) {
            this._thumb = thumb;
        }
    }]);

    return Item;
}();

/***/ })
/******/ ]);